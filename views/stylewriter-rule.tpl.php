<?php
/**
 * Style a Mapnik Rule
 *
 * @param $filter
 * @param $rules
 * @param $postfix
 */
?>
#data[<?php echo $filter; ?>]{
<?php if ($rules): ?>
  <?php foreach($rules as $k => $v): ?>
    <?php echo "$k: $v;"; ?>
  <?php endforeach; ?>
<?php endif; ?>

/* postfix */
<?php if($postfix) { echo $postfix; } ?>
}
