
/**
 * OpenLayers Zoom to Layer Behavior
 */
Drupal.behaviors.stylewriter_interaction = function(context) {
  var layers, data = $(context).data('openlayers');
  if (data && data.map.behaviors['stylewriter_interaction']) {
    map = data.openlayers;
    h = new OpenLayers.Control.StyleWriterInteraction({
      callbacks: {
        'over': Drupal.StyleWriterTooltips.select,
        'out': Drupal.StyleWriterTooltips.unselect,
        'click': Drupal.StyleWriterTooltips.click
      }
    });
    map.addControl(h);
    h.activate();
  }
};
