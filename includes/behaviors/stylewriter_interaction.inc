<?php

/**
 * @file
 * StyleWriter Point Interaction behavior
 */
class stylewriter_interaction extends openlayers_behavior {
  /**
   * Override of options_init().
   */
  function options_init() {
    $options = parent::options_init();
    return $options;
  }

  /**
   * Override of options_form().
   */
  function options_form() {
    $form = parent::options_form();
    return $form;
  }

  function js_dependency() {
    return array(
      'OpenLayers.Handler.Hover',
      'OpenLayers.Handler.Click'
    );
  }
  
  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'stylewriter') 
      .'/includes/behaviors/jquery.jsonp-2.1.2.min.js');
    drupal_add_js(drupal_get_path('module', 'stylewriter') 
      .'/includes/behaviors/stylewriter_tooltip.js');
    drupal_add_js(drupal_get_path('module', 'stylewriter') 
      .'/includes/behaviors/stylewriter_openlayers_interaction.js');
    drupal_add_js(drupal_get_path('module', 'stylewriter') 
      .'/includes/behaviors/stylewriter_interaction.js');
    return $this->options;
  }
}
